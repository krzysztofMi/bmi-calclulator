package bmi.controller;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BmiCalculatorController {
    @FXML
    CheckBox femaleCheck;
    @FXML
    CheckBox maleCheck;
    @FXML
    TextField size;
    @FXML
    TextField weigth;
    @FXML
    TextField anserw;
    @FXML
    Slider sizeSlider;
    @FXML
    Slider weigthSlider;

    public void initialize(){
        anserw.setMouseTransparent(true);


        size.focusedProperty().addListener((arg, oldVal, newVal)->{

            if(!size.getText().isEmpty()) {
                double newValue = Double.valueOf(size.getText());
                if (newValue < 30) {
                    sizeSlider.setValue(30);
                    size.setText("130");
                } else if (newValue > 250) {
                    sizeSlider.setValue(250);
                    size.setText("250");
                } else {
                    sizeSlider.setValue(newValue);
                }
            }
        });
        weigth.focusedProperty().addListener((arg, oldVal, newVal)->{
            if(!weigth.getText().isEmpty()){
                double newValue = Double.valueOf(weigth.getText());
                if (newValue < 30) {
                    weigthSlider.setValue(30);
                    weigth.setText("130");
                } else if (newValue > 250) {
                    weigthSlider.setValue(250);
                    weigth.setText("250");
                } else {
                    weigthSlider.setValue(newValue);
                }
            }
        });

        sizeSlider.valueProperty().addListener((arg, oldVal, newVal)->
                size.setText(Integer.toString((int) Math.round((double)newVal))));
        weigthSlider.valueProperty().addListener((arg, oldVal, newVal)->
                weigth.setText(Integer.toString((int) Math.round((double)newVal))));


    }

    public void maleClick(){
        femaleCheck.setSelected(false);
    }

    public void femaleClick(){
        maleCheck.setSelected(false);
    }

    public void calculate(){
        if(femaleCheck.isSelected() == false && maleCheck.isSelected() == false){
            //FEEDBACK
        }else
        if(size.getText().equals("") || weigth.getText().equals("")){
            //FEEDBACK
        }else{
            double weightD = Double.valueOf(weigth.getText());
            double sizeD = Double.valueOf(size.getText());
            sizeD /=100;
            double bmi = weightD/(sizeD*sizeD);
            bmi = round(bmi, 2);
            String anserwText = String.valueOf(bmi);
            if(bmi<18.5)
                anserwText = anserwText.concat(" UNDERWEIGTH");
            else if(bmi<25)
                anserwText = anserwText.concat(" GOOD WEIGTH");
            else
                anserwText = anserwText.concat(" OVERWEIGTH");
            anserw.setText(anserwText);
        }

    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
