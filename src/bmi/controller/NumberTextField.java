package bmi.controller;


import javafx.scene.control.TextField;

public class NumberTextField extends TextField {


    String matchesText = "[0-9]";

    @Override
    public void replaceText(int start, int end, String text)
    {
        if (validate(text))
        {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text)
    {
        if (validate(text))
        {
            super.replaceSelection(text);
        }
    }

    private boolean validate(String text)
    {
        return text.matches(matchesText);
    }

    public void setMatchesText(String matchesText){
        this.matchesText = matchesText;
    }
}
